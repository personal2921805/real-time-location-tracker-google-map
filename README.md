# Real-Time Location Tracker Using Google Map

Real-Time Location Tracker is a Flutter app that allows users to track their current location on a Google Map in real-time.

## Features

- Displays the user's real-time location on a Google Map.
- Updates the location in real-time as the user moves.
- Shows a polyline on the map to trace the user's movement history.
- Provides the latitude and longitude of the user's location in an InfoWindow on the map marker.
- Uses the `location` package for handling device location.
- Uses the `google_maps_flutter` package for displaying the map.

## Screenshots
<img src="https://gitlab.com/personal2921805/real-time-location-tracker-google-map/-/raw/main/first_look.png" width="180" />

<img src="https://gitlab.com/personal2921805/real-time-location-tracker-google-map/-/raw/main/with_infow_window.png" width="180" />

<img src="https://gitlab.com/personal2921805/real-time-location-tracker-google-map/-/raw/main/location_screen_wityh_path.png" width="180" />


## Getting Started

1. Clone the repository to your local machine:

   ```
   git clone https://gitlab.com/personal2921805/real-time-location-tracker-google-map.git
   ```

2. Change the working directory to the project folder:

   ```
   cd realtime_location_tracker
   ```

3. Install the required dependencies:

   ```
   flutter pub get
   ```

4. Run the app on a connected device or emulator:

   ```
   flutter run
   ```

## Dependencies

- [location](https://pub.dev/packages/location): A Flutter plugin for getting device location updates.
- [google_maps_flutter](https://pub.dev/packages/google_maps_flutter): A Flutter plugin for embedding Google Maps in a Flutter app.

## Contributing

If you find any issues or have suggestions for improvements, please feel free to open an issue or create a pull request in the [GitHub repository](https://gitlab.com/personal2921805/real-time-location-tracker-google-map).

## License

This project is licensed under the MIT License - see the [LICENSE](https://opensource.org/license/mit/) file for details.

