import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:location/location.dart';
import '../utils/location_formater_utils.dart';


class MapScreen extends StatefulWidget {
  const MapScreen({super.key});

  @override
  State<MapScreen> createState() => _MapScreenState();
}

class _MapScreenState extends State<MapScreen> {

  late GoogleMapController _myMapController;
  bool isLoading = false;
  LocationData? myLocation;
  final List<LatLng> _createPolylineCoordinates = [];
  final Set<Polyline> _polyLinesSet = {};
  late String infoWindowText;



  @override
  void initState() {
    super.initState();
    isLoading = true;
    getMyCurrentLocation();
  }

  void getMyCurrentLocation() async {
    await Location.instance.requestPermission().then((permissionRequested) {});
    await Location.instance.hasPermission().then((permissionStatus) {});
    myLocation = await Location.instance.getLocation();
    listenRealTimeLocation();
    setState(() {});
  }

  void listenRealTimeLocation() {
    Location.instance.onLocationChanged.listen((location) {
      if (location != myLocation) {
        myLocation = location;
        infoWindowText = "Lat: ${myLocation?.latitude ?? 0}, Lng: ${myLocation?.longitude ?? 0}";
        _updateMapPolyline();
        isLoading = false;
        setState(() {});
      }
    });
  }

  void _updateMapPolyline() {
    _createPolylineCoordinates.add(formatLocation(myLocation));
    _polyLinesSet.add(Polyline(
      polylineId: const PolylineId('tracking_polyline'),
      points: _createPolylineCoordinates,
      color: Colors.blue,
      width: 5,
    ));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Real Time Location Tracker'),
      ),
      body: isLoading
          ? const Center(
        child: CircularProgressIndicator(),
      )
          : GoogleMap(
        onMapCreated: (GoogleMapController controller) {
          _myMapController = controller;
          _myMapController.animateCamera(CameraUpdate.newLatLng(formatLocation(myLocation)));
        },
        initialCameraPosition: CameraPosition(
            target: formatLocation(myLocation),
            zoom: 20,
            tilt: 10,
            bearing: 30),
        myLocationEnabled: true,
        trafficEnabled: true,
        myLocationButtonEnabled: true,
        mapType: MapType.normal,
        markers: <Marker>{
          Marker(
            markerId: const MarkerId('Marker-1'),
            position: formatLocation(myLocation),
            icon: BitmapDescriptor.defaultMarkerWithHue(
                BitmapDescriptor.hueAzure),
            infoWindow: InfoWindow(
              title:
              "My Location is",
              snippet: infoWindowText,
            ),
            onTap: (){
              _myMapController.showMarkerInfoWindow(const MarkerId('Marker-1'));
            }
          ),
        },
        polylines: _polyLinesSet,
      ),
    );
  }
}
