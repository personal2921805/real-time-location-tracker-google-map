import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:location/location.dart';


LatLng formatLocation(LocationData? locationData) {
  final latitude = locationData?.latitude ?? 0;
  final longitude = locationData?.longitude ?? 0;
  return LatLng(latitude, longitude);
}

